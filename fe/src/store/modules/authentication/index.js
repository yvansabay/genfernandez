import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../config/config'

Vue.use(Vuex);

const adviser = 'adviser'

export default ({
  namespaced: true,
  state: {
    user: [],
    token: localStorage.getItem('auth') || '',
  },
  actions: {
    async LogInUser({commit}, user){
      const res = await AXIOS.post(`${adviser}/login`, user).then(response => {
        commit('SET_USER', response.data.user)
        commit('SET_TOKEN', response.data.access_token)

        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
    async checkUser({commit}){
      const res = await AXIOS.post(`${adviser}/me?token=` + localStorage.getItem('auth')).then(response => {
        commit('SET_USER', response.data)
        commit('SET_TOKEN', localStorage.getItem('auth'))
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
    async logoutUser({commit}){
      const res = await AXIOS.post(`${adviser}/logout?token=` + localStorage.getItem('auth')).then(response => {
        commit('UNSET_USER')
        return response
      }).catch(error => {
        return error.response
      });

      return res;
    }
  },
  getters: {
    getUser(state){
      return state.user;
    },
    getToken(state){
      return state.token;
    }
  },
  mutations: {
    SET_USER(state, user){

      state.user = user
    },
    SET_TOKEN(state, token){
      localStorage.setItem('auth', token)
      state.token = token

      const bearer_token = localStorage.getItem('auth') || ''
      AXIOS.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`
    },
    UNSET_USER(state){
      localStorage.removeItem('auth');
      state.token = ''

      AXIOS.defaults.headers.common['Authorization'] = ''
    }
  },
})