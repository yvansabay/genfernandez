<?php

use App\Http\Controllers\AdviserController;
use App\Http\Controllers\GenderController;
use App\Http\Controllers\SectionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\student_controller;
use App\Http\Controllers\StudentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'adviser', 'middleware'=>'api'], function () {
    Route::post('store', [AdviserController::class, 'store']);
    Route::post('me', [AdviserController::class, 'me']);
    Route::post('login', [AdviserController::class, 'login']);
    Route::post('logout', [AdviserController::class, 'logout']);
    Route::apiResource('student', StudentController::class);
});


Route::get('section', [SectionController::class, 'index']);
Route::get('gender', [GenderController::class, 'index']);




