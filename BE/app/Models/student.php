<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'first_name',
        'middle_name',
        'last_name',
        'section_id',
        'gender_id',
        'adviser_id',
    ];

    public function section(){
        return $this->belongsTo(Section::class, 'section_id', 'id');
    }

    public function gender(){
        return $this->belongsTo(Gender::class, 'gender_id', 'id');
    }

    public function adviser(){
        return $this->belongsTo(Adviser::class, 'adviser_id', 'id');
    }
}
