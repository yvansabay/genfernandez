<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AdviserAccount extends Authenticatable implements JWTSubject
{
    use HasFactory;

    protected $fillable = [
        'adviser_id',
        'email',
        'password'
    ];

    protected $hidden = [
        'password',
    ];
    
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function adviser(){
        return $this->belongsTo(Adviser::class, 'adviser_id', 'id');
    }
}
