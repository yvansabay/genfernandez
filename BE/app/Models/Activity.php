<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'adviser_id',
        'activity',
        'output',
    ];

    public function student(){
        return $this->belongsTo(Student::class, 'student_id', 'id');
    }
    
    public function adviser(){
        return $this->belongsTo(Adviser::class, 'adviser_id', 'id');
    }
}
