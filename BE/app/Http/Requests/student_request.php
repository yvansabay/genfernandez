<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class student extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_number' => ['integer', 'required', 'bail', 'unique', 'max:255'],
            'password' => ['required', 'bail', 'min:8'],
            'first_name' => ['required', 'bail', 'max:255'],
            'last_name' => ['required', 'bail', 'max:255'],
            'middle_name' => ['required', 'bail', 'max:255'],
            'department' => ['required', 'bail', 'max:255'],
            'year_level' => ['required', 'max:255'],
        ];
    }
}
