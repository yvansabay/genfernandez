<?php

namespace App\Http\Controllers;

use App\Models\Adviser;
use App\Models\AdviserAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class AdviserController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt', ['except' => ['store', 'login']]);
    }

    public function me()
    {
        return response()->json(AdviserAccount::with(['adviser', 'adviser.gender:id,gender'])->find(Auth::id()));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'bail|required',
            'middle_name' => 'bail|required',
            'last_name' => 'bail|required',
            'department' => 'bail|required',
            'position' => 'bail|required',
            'gender_id' => 'bail|required',
        ]);

        $this->validate($request, [
            'email' => 'bail|required|email|unique:adviser_accounts',
            'password' => 'bail|required|min:6',
        ]);

        $adviser = Adviser::create([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'department' => $request->department,
            'position' => $request->position,
            'gender_id' => $request->gender_id
        ]);
        
        AdviserAccount::create([
            'adviser_id' => $adviser->id,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        return response()->json(['success' => 'Account created successfuly!'], 200);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);

        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }

    public function logout()
    {
        JWTAuth::invalidate(Request()->token);
        auth()->logout();
        return response()->json(['message' => 'User logged out successfully!']);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => AdviserAccount::with(['adviser', 'adviser.gender'])->where('id', Auth::id())->first()
        ])->header('Authorization: Bearer ', $token);
    }

    public function payload()
    {
        return auth()->payload();
    }
}
