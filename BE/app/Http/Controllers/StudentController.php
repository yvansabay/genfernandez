<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt');
    }

    public function index(){
        $student = Student::with(['gender', 'section'])->where('adviser_id', Auth::id())->get();
        return response()->json($student);
    }

    public function store(Request $request){
        Student::create($request->all());
        return response()->json(['msg' => 'Student added successfully!'], 200);
    }
}
