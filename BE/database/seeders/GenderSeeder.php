<?php

namespace Database\Seeders;

use App\Models\Gender;
use Illuminate\Database\Seeder;

class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gender = [
            ['gender' => 'Male'],
            ['gender' => 'Female'],
        ];

        foreach ($gender as $g){
            Gender::create($g);
        }
    }
}
