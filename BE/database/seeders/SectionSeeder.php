<?php

namespace Database\Seeders;

use App\Models\Section;
use Illuminate\Database\Seeder;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $section = [
            ['section' => 'AI4-1'],
            ['section' => 'AI4-2'],
            ['section' => 'AI4-3'],
            ['section' => 'AI4-4'],
        ];

        foreach ($section as $s){
            Section::create($s);
        }
    }
}
